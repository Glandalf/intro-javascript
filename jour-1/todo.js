
let taskList = []
showTasks()

function addTask() {
    // Récupérer le texte de l'input
    const value = document.querySelector('#field').value
    console.log('addTask', value)
    
    // L'ajouter à la liste
    // const task = document.createElement('li')
    // task.classList.add('item')
    // task.innerText = value
    // document.querySelector('#todo-list').prepend(task)

    // On l'ajoute à notre liste
    taskList.push(value)
    // Et on sauve la liste dans le localStorage
    window.localStorage.setItem('list', taskList)

    // Vider le input
    document.querySelector('#field').value = ''
}

function showTasks() {
    // Récupère depuis le localStorage et les stocke dans taskList
    taskList = window.localStorage.getItem('list').split(',')

    // les affiche dans le HTML
    console.log(taskList)
    // On utilise innerHTML pour vider le ul, attention au XSS avec cette méthode
    document.querySelector('#todo-list').innerHTML = ''
    taskList.forEach(function (label) {
        const li = document.createElement('li')
        li.classList.add('item')
        li.innerText = label
        document.querySelector('#todo-list').prepend(li)
    })
}

// On sélectionne le bouton du formulaire
document.querySelector('#todo-form')
    // On y écoute l'événement de click
    // Au clic, on "prevent" (annule) l'événement par défaut, 
    // puis on ajoute l'élément à la liste 
    .addEventListener('submit', function (ev) {
        ev.preventDefault()
        addTask()
        showTasks()
        document.querySelector('#field').focus()
    })