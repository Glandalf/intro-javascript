'use strict'

/*****************************************
 * 
 * Lecture dans le HTML
 * 
 */
// Récupérer un éléments par son nom de balise 
document.querySelector('div')
document.querySelector('h1')
// Récupérer tous les éléments par le nom de balise 
// document.getElementsByTagName('div')
document.querySelectorAll('div')
document.querySelector('button')
// Récupérer par ID
// document.getElementById('toto')
document.querySelector('#toto')
// Récupérer par classes
// document.getElementsByClassName('maclasse')
document.querySelector('.ma-classe')
document.querySelectorAll('.ma-classe')
// Récupérer les paragraphes dans le header
document.querySelectorAll('header p')

// ... Voir query Selectors et Emmet







/*****************************************
 * 
 * Lecture dans le HTML
 * 
 */

// Déclaration de variable + affectation d'un élément HTML de type h1
let tag = document.querySelector('h1')
// Affichage de cet élément HTML
console.log(tag)
// affichage du nom de balise
console.log(tag.tagName)
// Affichage du contenu de la balise
console.log(tag.innerText)

//Réaffectation (élément de type h2)
tag = document.querySelector('h2')
// On réaffiche le tag
console.log(tag)
// On affiche la valeur de l'attribut "class"
console.log(tag.getAttribute('class'))

// On peut afficher des jolis tableaux dans la console
// console.table([['nom','niveau','points'],['tito','2','20'],['pipou','999','9999'],])








/*****************************************
 * 
 * Ecriture dans le HTML
 * 
 */
// On peut modifier le contenu du titre
document.querySelector('h1').innerText += '🐞'
// On peut ajouter des classes CSS
document.querySelector('h1').classList.add('javascript-css')
// On peut aussi en supprimer  (même si l'élément ne l'a pas de base)
document.querySelector('h1').classList.remove('pa-cet-class')
// document.querySelector('h1')
// document.querySelector('h1')


// On crée un élément de type img
let imageTag = document.createElement('img')
// On lui affecte un attribut src
imageTag.setAttribute('src', 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.6Ajf_rdfbycBeZl-ORV5iQHaE9%26pid%3DApi&f=1&ipt=7ff19219bdac108f07b2874df4a5664bfdd6428754604f8ec01664e237dd1f8f&ipo=images')
// On lui affecte un attribut alt
imageTag.setAttribute('alt', 'cé un lapin')

// A ce stade, elle n'est que dans le virtual DOM
// On peut alors l'attacher au DOM : 
document.body.append(imageTag)
