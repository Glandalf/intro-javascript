function getPokemon(pokemonName) {
    fetch('https://pokeapi.co/api/v2/pokemon/' + pokemonName)
        .then(response => response.json())
        .then(data => {
            const weight = data.weight
            const name = data.species.name
            const types = data.types

            document.getElementById('name').innerText = name
            document.getElementById('weight').innerText = weight + 'kg'
            for (let type of types) {
                const li = document.createElement('li')
                li.classList.add('type')
                li.innerText = type.type.name
                document.getElementById('types').append(li)
            }
        })
        .catch()
}

document.getElementById('pokemon')
    .addEventListener('keypress', (ev) => {
        if (ev.key === 'Enter') {
            getPokemon(document.getElementById('pokemon').value)
        }
})