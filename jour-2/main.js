let score = 0
let obstacles = []
let isRunning = false
let gameState = 'lobby'  // lobby | pending | game-over 
// Hauteur minimum pour éviter un obstacle
// const MIN_SAFE_JUMP_HEIGHT= 50
const MIN_SAFE_Y_POSITION= 220

let dinosaurElement = document.getElementById('dinosaur')
let scoreElement = document.getElementById('score')

function run() {
    if (dinosaurElement.getAttribute('src') === 'dino-1.png') {
        dinosaurElement.setAttribute('src', 'dino-2.png')
    } 
    else {
        dinosaurElement.setAttribute('src', 'dino-1.png')
    }
}

function jump() {
    dinosaurElement.classList.add('jump')
    setTimeout(function() { dinosaurElement.classList.remove('jump') }, 900)
}

function start() {

}

function isTouched() {
    let before = dinosaurElement.getBoundingClientRect().x
    let after = dinosaurElement.getBoundingClientRect().x + dinosaurElement.getBoundingClientRect().width
    let dinoHeight = dinosaurElement.getBoundingClientRect().y 
    // Si le dino a sauté assez haut, il est safe : 
    if (dinoHeight < MIN_SAFE_Y_POSITION) {
        return false
    }
    
    // récupérer tous les obstacles
    const obstacles = document.getElementsByClassName('obstacle')
    // Pour chaque obstacle : voir is sa position en X correspond à celle du dino
    for(let obstacle of obstacles) {
        const pos = obstacle.getBoundingClientRect()
        if (pos.x + pos.width > before && pos.x < after) {
            return true
        }
    }
    // si j'ai pas rencontré d'obstacle
    return false

}

function loop() {
    console.log(dinosaurElement.getBoundingClientRect().y)
    if (gameState === 'lobby') {

    }
    else if (gameState === 'pending') {
        run()
        score += 1
        scoreElement.innerText = score
        generateObstacle()
        if (isTouched()) {
            gameState = 'game-over'
        }
    }
    else if (gameState === 'game-over') {
        alert('Votre score est de ' + score)
        score = 0
        const obstacles = document.getElementsByClassName('obstacle')
        for(let obstacle of obstacles) { obstacle.remove() }
        gameState = 'pending'
    }
    setTimeout(loop, 50)
}

function generateObstacle() {
    if (Math.random() > .96) {
        createCactus()
    }
}

function createCactus() {
    const cactusElement = document.createElement('img')
    cactusElement.classList.add('obstacle')
    cactusElement.classList.add('cactus')
    cactusElement.setAttribute('src', 'cactus.png')
    cactusElement.setAttribute('alt', 'obstacle')
    document.getElementById('game').append(cactusElement)
}





document.body.addEventListener('keydown', function(ev) {
    console.log(ev.key)
    if (gameState !== 'pending') {
        if (ev.key === ' ') {
            gameState = 'pending'
            loop()
        }
    }
    else {

        // si c'est espace  ET que on est en game : on saute
        if (ev.key === ' ') {
            jump()
        }
    }
})