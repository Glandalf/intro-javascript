# Cours Web Javascript

## Contenu du module

### Jour 1 - prise en main

Découverte de la place de Js dans le navigateur
- HTML est un langage de structuration de contenu
- CSS un langage de mise en forme
- JavaScript un langage de programmation

JavaScript permet notamment de manipuler le *Document Object Model*, en lisant le contenu d'une page, en en modifiant les élément existants, et même en créant ou supprimant des éléments.

#### Dans une console du navigateur

Nous avons découvert la syntaxe de Javacript : 
- **déclaration de variables** avec `let` ou `const` mais JAMAIS avec le mot clé dont on ne doit pas dire le nom
- **affectation** de valeur à des variables
- les **opérateurs** courants comme + pour additionner ou concaténer mais aussi des opérateurs de comparaison comme < <= > >= === !=== etc.
- revu les éléments de logique booléenne simple


#### Dans le fichier [main.js](./jour-1/main.js)

Nous avons vu les rudiments du JavaScript pour navigateur : 
- comment inclure un fichier js
- les query selectors
- lire du contenu de la page HTML
- modifier et ajouter du contenu


#### Dans le fichier [todo.js](./jour-1/todo.js)

Et plus généralement, dans les 3 fichiers "todo", nous avons codé une petite application de Todo list, en suivant le plan d'attaque suivant : 
- une page HTML rudimentaire
- une fonction JS permettant d'ajouter à notre liste le contenu d'un input
- un eventListener sur le formulaire pour déclencher l'ajout d'une tâche à chaque "post" du formulaire
- une fonction qui affiche la liste de toutes les tâches (on appelle cette fonction une fois au démarrage, puis à chaque ajout)
- on a même sauvegardé tout ça dans un local storage : on conserve nos tâches même en éteignant l'ordi <3



### Jour 2 - coder un petit jeu 🌵 🦖

Dans ce cours, nous avons continuer de mettre en application ce que l'on a commencé au cours précédent afin de manipuler le DOM.

Pour faire cela, nous avons créé un petit jeu runner à base de cactus et de dinosaures, sans utiliser de canvas (approche plus pertinente dans l'absolu) afin de continuer de manipuler les outils du DOM.

Nous avons donc pour cela construit une machine à **état transition** construite comme suit : 
- un lobby depuis lequel on peut lancer la première partie
- un pending pendant lequel le gros de l'action se passe
- un game over qui nous invite à relancer

Cela peut se représenter sous forme d'un diagramme :

![alt text](image.png)
> On aurait pu repasser par un état "lobby" après avoir perdu, nous avons opté pour un redémarrage de partie direct

Pour gérer un jeu de manière générale, il est souvent une bonne idée de créer une "boucle" qui tourne en permanence et qui va réaliser les différentes actions selon les états.

Nous sommes parti sur une fonction `loop()` qui s'appelle de manière récursive toutes les 50ms en gros (environ 20 fois par seconde donc) et qui gère tout notre jeu.

A chaque itération de la boucle, on crée potentiellement des obstacles (`Math.random`), on fait courir le dinosaure, on incrémente le score et on vérifie s'il n'y a pas eu de collision. 

On utilise deux types d'animations : le saut et le déplacement des cactus, gérés en CSS avec des keyframes. Tout le positionnement de nos élément est donc géré en CSS.

Pourtant on peut vérifier le positionnement de nos éléments en JS, notamment pour détecter les collisions, à l'aide d'une fonction très pratique : `getBoundingClientRect` qui permet notamment de récupérer la position, et la taille d'un objet. En comparant la position des obstacles avec celle du dinosaure, on peut savoir s'il y a collision ou pas.



### Jour 3 - protocole HTTP et requêtes fetch

Nous avons découvert le protocole HTTP dans les grandes lignes.
On sait désormais que tout cela fonctionne sur un principe de requête-réponse : le client (navigateur en général) envoie une requête vers une serveur, ce dernier traite la requête, puis produit une réponse que le client attend.


![alt text](image-1.png)


Le serveur, en HTTP, ne peut pas de son initiative contacter le client (on utilisera d'autres protocoles comme les web sockets).

Une requête est essentiellement constituée de 4 éléments : 
- un verbe (ou une méthode)
- une URL
- des headers
- un body optionnel 

![alt text](image-2.png)

Il faudrait toujours définir un "contrat d'API" définissant tous les formats de requêtes de l'application, et toutes les réponses possibles. 

Des outils comme Swagger (designer d'API), Postman/Insomnia/Hoppscotch (requêteur frontend), Mockoon (simulateur de serveur) reposent sur un format de "contrat d'API" nommé **Open API**. Ce standard est le plus communément utilisé de nos jours.


Côté client, on utilise JavaScript pour produire des requêtes HTTP. Cela se fait à l'aide de l'API `fetch`, qui permet d'envoyer des requêtes, spécifiant pour chacune la méthode, l'URL, les headers et le body.

En même temps que nous introduisons `fetch`, nous amenons le concept de fonctionnalités fournies par le navigateur et qui fonctionnent en asynchrone (c'est le cas de fetch donc).

![alt text](image-3.png) 

> On aime l'asynchrone, ce n'est jamais compliqué

Travailler en asynchrone, dans notre cas avec des promesses, signifie que tout le code qui se trouve après notre fetch va s'exécuter avant que la requête n'ait été exécuté.

Cela signifie que pour travailler AVEC le résultat de la requête, il va falloir faire autrement : indiquer à JS d'attendre la réponse du fetch pour exécuter cette partie du code. Et cela se fait à l'aide de la méthode `then` de la promesse retournée par `fetch`.
Voir dans le fichier [requetes-fetch](./jour-3/requetes-fetch.js) des exemples de requêtes fetch qui décrivent un callback dans la partie then, amusez vous avec, en essayant d'en modifier le contenu par exemple).

> Note : pour un fetch, vous voyez que l'on utilise deux `then`... 
> 
> C'est parce que l'extraction du contenu en JSON retourne également une promesse, il va donc falloir faire avec ce code qui bégaie un peu...

On a ensuite joué avec une API Pokémon disponible gratuitement en ligne (dans le fichier [main.js](./jour-3/main.js)) pour trouver des Pokémon et en afficher quelques propriétés basiques.






## Ressources HTML


- 🍿 [Cours de Grafikart](https://www.youtube.com/playlist?list=PLjwdMgw5TTLUeixVGPNl1uZNeJy4UY6qX)
- 🍿 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 Cours HTML & CSS de Kevin POWELL](https://www.youtube.com/playlist?list=PL4-IK0AVhVjM0xE0K2uZRvsM7LkIhsPT-)

## Ressources CSS

- 🍿 [Cours de Grafikart](https://www.youtube.com/playlist?list=PLjwdMgw5TTLVjTZQocrMwKicV5wsZlRpj)
- 🍿 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 Shorts YT de Kevin POWELL](https://www.youtube.com/playlist?list=PL4-IK0AVhVjNVnguR9gRrXumxaIGsRx75)
- 🎮 [Jeu Grid Garden CSS](https://cssgridgarden.com/#fr)
- 🎮 [Jeu Flexbox Froggy CSS](https://flexboxfroggy.com/#fr)


## Ressources JavaScript

Les ressources sont peut être ici classées par ordre croissante de difficulté

- 🍿 [Cours de Grafikart](https://www.youtube.com/playlist?list=PLjwdMgw5TTLXgsTQE_1PpRkC_yX47ZcGV)
- 🍿 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 Cours de Wes BOS](https://www.youtube.com/playlist?list=PLu8EoSxDXHP5H-e74dRVbl2Tj76RagB74)
- 🍿 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 Chaine de Lydia HALLIE](https://www.youtube.com/@theavocoder/featured)
- 🎮 [Onboarding de CodinGame](https://www.codingame.com/ide/puzzle/onboarding)
  *pendant l'onboarding, choisir le langage JavaScript*
- 📖 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 You don't know JS](https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/get-started/toc.md)
  un livre de référence, en anglais et surtout Open source, consultable sur Github (vous pouvez l'acheter en physique pour supporter son travail si vous voulez/pouvez)


Pour aller plus loin
- 📚 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 Plein de ressources JS avancées](https://github.com/airbnb/javascript)
  Guide de. bonnes pratiques autour de JS par AirBnB, oui, cette entreprise.
- 📚 [🏴󠁧󠁢󠁥󠁮󠁧󠁿 Plein de ressources JS avancées](https://github.com/leonardomso/33-js-concepts?tab=readme-ov-file#-table-of-contents)
  Globalement très avancé, inutile de s'y aventurer dans le cadre de ce cours, sauf pour les gens qui s'ennuient. Je n'ai pas pu tout consulter mais WOW.
